import '../../config/node_modules/regenerator-runtime/runtime';

import {contentAnimationIn} from './scripts/animationTitle';
import './scripts/functions';
import './scripts/updatecss';
import {navigation} from './scripts/navigation';
import './scripts/swiper';
import './scripts/form';
import {filter} from './scripts/filter'; 
import {general} from './scripts/general';
import '../child_theme/src/scripts/script';
import {lazyload} from './scripts/lazyload';
import {calculator} from './scripts/calculator';
import { authentication } from './scripts/authentication/authentication';
import './styles/style';



contentAnimationIn();
navigation();
filter();
general();
lazyload(document);
calculator();
authentication();