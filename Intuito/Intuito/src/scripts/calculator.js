

async function calculator () {
    var elements = document.querySelectorAll(".form-field-calc input")
    var $ = require("../../../config/node_modules/jquery");
    $.prototype.disableTab = function() {
        $(this).each(function() {
            $(this).attr('tabindex', '-1');
        });
    };
    
    setInterval(function(){ 
        $('.disabled input').disableTab();
    }, 200);   

    for (var i = 0, element; element = elements[i++];) {
        element.addEventListener('change', fieldChange);
    }

    function fieldChange(){
        var fieldId = this.getAttribute('id');
        var rate = document.getElementById('rate');
        var hourPrice = document.getElementById('hour');
        var bonusRange = document.getElementById('bonus');
        var salary = document.getElementById('salary');
        var revenue = document.getElementById('revenue');
        var bonus = document.getElementById('bonus_price');
        var basisBruto = parseFloat(document.getElementById('calculator').getAttribute('basis'));
        var basisRate = parseFloat(document.getElementById('calculator').getAttribute('factor'));
        var invoiceHours = parseFloat(document.getElementById('calculator').getAttribute('invoice'));

        // var value = this.value;
        var rateValue = rate.value.replace('€', '').replace(/[^0-9]+/, '');
        
        hourPrice.value = hourPrice.value.replace(/[^0-9]+/, '');
        if (between(hourPrice.value, 0, 80)) {
            hourPrice.value = hourPrice.value;
        } else {
            hourPrice.value = 80;
        }

        // var hourpriceValue = hourPrice.value.replace(/[^0-9]+/, '');

        if (fieldId === 'rate'){ 
            var bonusRanges = bonusRange.parentElement.getAttribute('range').split('||');
            for (var i = 0, range; range = bonusRanges[i++];) {
                var rangeSplit = range.split('(');
                var rangeSplitRange = rangeSplit[0].split('>');
                var rangeSplitPercentage = rangeSplit[1].split(')');
                if (between(rateValue, parseInt(rangeSplitRange[0]), parseInt(rangeSplitRange[1]))) {
                    bonusRange.value = parseInt(rangeSplitPercentage[0]) + '%';
                }
            };
        }

        var invoiceBonus = parseFloat(hourPrice.value) * parseFloat(rateValue);
        var invoiceBruto = parseFloat(invoiceBonus * parseFloat('0.' + parseFloat(invoiceHours))); // console.log({invoiceBonus})

        // OMZET 
        var revenueFormula = invoiceBruto * basisRate; // console.log({revenueFormula})
        
        // BONUS 
        var bonusPrice = parseFloat('0.' + parseFloat(bonusRange.value.replace("%",""))) * revenueFormula; // console.log({bonusPrice})
        
        // BRUTO 
        var brutoPrice = basisBruto + bonusPrice;
            
        // UURTARIEF 
        rate.value = '€' + rateValue;
        
        if (isNaN(brutoPrice) === false){
            salary.value = '€' + Math.ceil(parseInt(brutoPrice)/100) * 100;
            revenue.value = '€' + Math.ceil(parseInt(revenueFormula)/100) * 100;
            bonus.value = '€' + Math.ceil(parseInt(bonusPrice)/100) * 100;
        } else {
            if (rateValue !== ''){
                salary.value = '€' + parseFloat(rateValue);
                revenue.value = '€' + Math.ceil(parseInt(rateValue)/100) * 100;
                bonus.value = '€' + Math.ceil(parseInt(rateValue)/100) * 100;
            } else {
                salary.value = '€' + 0;
                revenue.value = '€' + 0;
                bonus.value = '€' + 0;
            }
        }
    }

    function between(x, min, max) {
        return x >= min && x <= max;
    }
} export {
    calculator
}
