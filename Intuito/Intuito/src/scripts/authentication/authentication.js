
async function authentication() {
    
    var $ = require("../../../../config/node_modules/jquery");

    $( ".sidebar__button" ).click(function() {
       ShowHide($(this));
    });

    function ShowHide(element) {
        let accountTitle = $('h1.account');
        let newAccountTitle = $(element).attr('title');

        // console.log()
        accountTitle.html(newAccountTitle)
        if ($(element).attr("item") == "mail") {
            $("#mail").fadeIn(500);
            $("#login").fadeOut(100);
            $("#contact").fadeOut(100);
        } else if ($(element).attr("item") == "login") {
            $("#contact").fadeIn(500);
            $("#mail").fadeOut(100);
            $("#login").fadeOut(100);   
        } else if ($(element).attr("item") == "password") {
            $("#login").fadeIn(500);
            $("#mail").fadeOut(100);
            $("#contact").fadeOut(100);
        } 
    }

 } export {
    authentication
 }
