// Import Swiper and modules
import {
	Swiper,
	Navigation,
	Pagination,
	Scrollbar,
	Controller,
	Mousewheel,
	Autoplay,
	EffectFade,
} from '../../../config/node_modules/swiper/swiper.esm';

Swiper.use([Navigation, Pagination, Scrollbar, Controller, Mousewheel, Autoplay, EffectFade]);

var proj_swiper = new Swiper('.project_slider', {
	slidesPerView: 1,
	spaceBetween: 20,
	scrollbar: {
	  el: '.swiper-scrollbar',
	  hide: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 3,
			slidesPerGroup: 3,
        },
	}
});
var rela_swiper = new Swiper('.related__products', {
	slidesPerView: 1,
	spaceBetween: 20,
	scrollbar: {
	  el: '.swiper-scrollbar',
	  hide: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 3,
			slidesPerGroup: 1,
        },
	},
});

var prod_swiper = new Swiper('.product_slider', {
	slidesPerView: 1,
	spaceBetween: 20,
	scrollbar: {
	  el: '.swiper-scrollbar',
	  hide: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 1,
			slidesPerGroup: 1,
        },
	}
});



var tesm_swiper = new Swiper('.testimonial_slider', {
	slidesPerView: 1,
	spaceBetween: 20,
	slidesPerGroup: 1,
	loopFillGroupWithBlank: false,
	pagination: {
	  el: '.swiper-pagination-testimonial',
	  clickable: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 3,	
        },
	}
});

var revi_swiper = new Swiper('.review_slider', {
	slidesPerView: 1,
	spaceBetween: 40,
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
	},
	mousewheel: {
		forceToAxis: true, 
	},
	effect: 'fade',
	fadeEffect: {
	  crossFade: true
	}
});

var usplist_swiper = new Swiper('.upslist_slider', {
	slidesPerView: 1,
	spaceBetween: 40,
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
	},
	mousewheel: {
		forceToAxis: true, 
	},
	effect: 'fade',
	fadeEffect: {
	  crossFade: true
	},
	loop: true,
	pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
          return '<span class="' + className + '">' + (index + 1) + '</span>';
        },
	},
})

usplist_swiper.on('slideChange', function () {
	
	var activeSlide = parseInt(document.getElementsByClassName('upslist_slider')[0].querySelector('.swiper-pagination-bullet-active').innerHTML) - 1;
	var activeWrap = '.wrap-' + activeSlide;
	
	ActiveWrap(activeWrap);
});

function ActiveWrap(active) {

	const wraps = document.querySelectorAll('.wrap');
	const activeWraps = document.querySelectorAll(active);

	wraps.forEach(wrap => {
		wrap.classList.remove('active-wrap')
	});
	activeWraps.forEach(wrap => {
		wrap.classList.add('active-wrap');
	});

}

var head_swiper = new Swiper('.header_slider', {
	slidesPerView: 1,
	spaceBetween: 0,
	pagination: {
	  el: '.swiper-pagination-header',
	  clickable: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	autoplay: {
        delay: 8000,
	},
	loop: true
});



var imga_swiper = new Swiper('.images_slider.no-loop', {
	slidesPerView: 1,
	spaceBetween: 20,
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
	},
	mousewheel: {
		forceToAxis: true, 
	},
});

var imga_swiper_loop = new Swiper('.images_slider.loop', {
	slidesPerView: 1,
	spaceBetween: 20,
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
	},
	mousewheel: {
		forceToAxis: true, 
	},
	loop: true,
});

