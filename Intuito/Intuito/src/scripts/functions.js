
var $ = require("../../../config/node_modules/jquery");

$(document).ready( function () {
    if(parent.document.getElementById('config-bar')) {
        $('html').addClass('config-mode');
    } else {
    }
    if ($("#colors").length > 0){ 
        rowColorFill();
    }
    if ($(".password-check").length > 0){ 
        showPassword()
    }
    
    if($('.grid .galerij-image')[0]){
        setInterval(function(){
            $('.grid .galerij-image').each(function (i, element) {        
                resizeGridItem(element);
            });
        }, 300);
    }
})

$(".scrolldown").click(function() {
    var navHeight = $('.nav').height();
    $('html,body').animate({
        scrollTop: $("section").first().offset().top - navHeight
    }, 'fast');
});

$('.card').on('click', function(){
    if(!$(this).hasClass('active') ){  
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
    } else {
        $(this).removeClass('active'); 
    }    
});


$(window).scroll(function() {
    var wScroll = $(window).scrollTop();
    $(".horizontal-text").each(function(){
        // console.log($(this).offset().top)
        $(this).css({
            transform: "translateX(-" + wScroll / 23 + "%)  translateY(-50%)"
        })    
    })
    
});

function rowColorFill() {
    var rowColors = $('#colors').attr('data-color');
    var rowColorsSingle = rowColors.split('||');

    // console.log(rowColorsSingle);

	$('.project-tray .plate--row').each(function(i){
        var rowColorClass = 'color__background';
        $(this).append('<div class="' + rowColorClass + '"></div>');
        
        // console.log($(this).find('.color__background').css('background', rowColorsSingle[i]))    
    });
}

function resizeGridItem(element) {
    let item = $(element).outerHeight(true);
    let grid = document.getElementsByClassName("grid")[0];
    let rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
    let rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
    let rowSpan = Math.ceil((item + rowGap) / (rowHeight + rowGap));
    element.style.gridRowEnd = "span " + rowSpan;
}

$(".thumbnail").click(function() {
    $(this).addClass('active-vid')
});

export const setActive = (el, active) => {
    const formField = el.parentNode;
    if (active) {
        formField.classList.add('form-field--is-active');
    } else {
        formField.classList.remove('form-field--is-active');
        el.value === '' ?
            formField.classList.remove('form-field--is-filled') :
            formField.classList.add('form-field--is-filled');
    }
};

