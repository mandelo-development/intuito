
import {setActive} from './functions';
import '../../../config/node_modules/bootstrap/js/dist/collapse';
import CircleType from '../../../config/node_modules/circletype';

var $ = require("../../../config/node_modules/jquery");

async function general() {
    const circleType1 = new CircleType(document.getElementById('slice-0'));
    circleType1.radius(235);
    const circleType2 = new CircleType(document.getElementById('slice-1'));
    circleType2.radius(235);
    const circleType3 = new CircleType(document.getElementById('slice-2'));
    circleType3.radius(235);
    const circleType4 = new CircleType(document.getElementById('slice-3'));
    circleType4.radius(235);
    const circleType5 = new CircleType(document.getElementById('slice-4'));
    circleType5.radius(235);
    const circleType6 = new CircleType(document.getElementById('slice-5'));
    circleType6.radius(235);
    const circleType7 = new CircleType(document.getElementById('slice-6'));
    circleType7.radius(235);
    const circleType8 = new CircleType(document.getElementById('slice-7'));
    circleType8.radius(235);
    const circleType9 = new CircleType(document.getElementById('slice-8'));
    circleType9.radius(235);
    const circleType10 = new CircleType(document.getElementById('slice-9'));
    circleType10.radius(235);
    const circleType11 = new CircleType(document.getElementById('slice-10'));
    circleType11.radius(235);
    const circleType12 = new CircleType(document.getElementById('slice-11'));
    circleType12.radius(235);
    const circleType13 = new CircleType(document.getElementById('slice-12'));
    circleType13.radius(235);
    const circleType14 = new CircleType(document.getElementById('slice-13'));
    circleType14.radius(235);
    const circleType15 = new CircleType(document.getElementById('slice-14'));
    circleType15.radius(235);
    const circleType16 = new CircleType(document.getElementById('slice-15'));
    circleType16.radius(235);

    $(document).ready( function () {
        /* FORM LABELS */ 
        [].forEach.call(
            document.querySelectorAll('.form-field__label, .form-field input, .form-field__textarea'),
            el => {
                el.onblur = () => {
                    setActive(el, false);
        
                };
                el.onfocus = () => {
                    setActive(el, true);
                };
        });
    });
    
} export {
    general
}