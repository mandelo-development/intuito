Steps to start Webpack:
1. Edit themefolderName to the name name of the template folder.
2. Edit themeUrl to the url of the preview website on Plate.
3. Install node modules in the config folder "yarn install" / "npm install".
4. Run webpack with "yarn watch" / "npm watch".
